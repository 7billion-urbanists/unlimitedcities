import { MAPBOX_CONFIG } from '../config'
const endpoint = "mapbox.places";

export function reverseGeocoding({lng, lat}){
    return new Promise((resolve, reject) => {
        fetch(`https://api.mapbox.com/geocoding/v5/${endpoint}/${lng},${lat}.json?access_token=${MAPBOX_CONFIG.accessToken}`)
        .then(response => response.json())
        .then(data => {resolve(data)})
        .catch(err => {reject(err)})
    });
}