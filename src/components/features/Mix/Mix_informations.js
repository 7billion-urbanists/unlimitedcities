/*
Path : /src/components/features/Mix

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form'
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MixHeader from './MixHeader';

import { getTranslate } from 'react-localize-redux';
import { saveMix, setMixUpload, setMixInformations } from '../../../actions';

import NextButton from './NextButton';

import informationsIcon from '../../../img/mix-header-informations.svg';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  position: relative;
  width: 100%;
  height:100%;
  background: radial-gradient(#5E5E5E,#2B2B2B);
  border: 10px solid white;
  h2{
    font-size: 20px;
    font-weight: 600;
  }
  .icon{
    display: flex;
    align-items: center;
    justify-content: center;
    border:1px solid white;
    border-radius: 8px;
    height: 50px;
    width: 50px;
  }
  form{
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    .form-container{
      position:relative;
      background: #F4F4F4;
      border-radius: 4px;
      display: flex;

      > div {
        margin: 15px;
      }

      > div > div {
        >label{
            font-size: 16px;
            display: block;
            padding-bottom:5px;
        }
        > div {
          > label{
            display: block;
            background: #fff;
            margin: 5px 0;
            padding: 8px;
            border-radius: 4px;
            font-size: 12px
          }

          > select{
            width: 100%;
            height: 37px;
            font-size: 16px;
            border-radius: 4px;
            border-width: 0px;
            background-color: white;
          }
        }
        padding-bottom: 20px;
        &:last-child{
            padding-bottom: 0;
        }
      }

      input, textarea{
        &[name=age]{
          border-radius: 4px;
          border: none;
          width: calc(100% - 20px);
          padding: 10px 10px;
          margin-top: 5px;
        }
        font-size: 16px;
        padding: 15px;
        &.error{
          border: #C74242 1px solid;
        }
      }
    }
  }
`;

const required = value => value ? undefined : 'Required'

class MixInformations extends React.Component {

  constructor(props){
    super(props);
    this.onBackClick = this.onBackClick.bind(this);
  }

  validateDataUpstream(){
    const {background, resultImage, location, comment, nickname} = this.props.mix;
    return background && resultImage && location && comment && nickname;
  }

  componentDidMount(){
    if(!this.validateDataUpstream()){
      this.props.history.push("/mix/comment");
    }
  }


  onBackClick(){
    this.props.history.push("/mix/comment");
  }

  onSubmit(values){
    this.props.setMixInformations(values);
    this.props.setMixUpload();
    this.props.history.push("/mix/save");
    this.props.saveMix({
      ...this.props.mix,
      extraForm: values
    }, this.props.user,
    () => {
      this.props.history.push("/map");
    }, () => {
      console.log("errorCallback");
      this.props.history.push("/mix/informations");
    });
    
  }

  /////////

  render(){
    const { handleSubmit, submitting, invalid, translate } = this.props;

    return(
      <Wrapper>
        <MixHeader
          onBackClick={this.onBackClick}
          title={translate('form')}
          icon={informationsIcon}
        />
        <form onSubmit={handleSubmit(
            this.onSubmit.bind(this)
          )}>
          <div className="form-container">
            <div>
              <div>
                <label>{translate('extra-form.age')}</label>
                <Field
                  name="age"
                  type="number"
                  component="input"
                />
              </div>
              <div>
                  <label>{translate('extra-form.user')}</label>
                  <div>
                      <label>
                          <Field
                          name="user"
                          component="input"
                          type="radio"
                          value="woman"
                          validate={[required]}
                          />{translate('extra-form.woman')}
                      </label>
                      <label>
                          <Field
                          name="user"
                          component="input"
                          type="radio"
                          value="youth"
                          validate={[required]}
                          />{translate('extra-form.youth')}
                      </label>
                      <label>
                          <Field
                          name="user"
                          component="input"
                          type="radio"
                          value="child"
                          validate={[required]}
                          />{translate('extra-form.child')}
                      </label>
                      <label>
                          <Field
                          name="user"
                          component="input"
                          type="radio"
                          value="Person with reduced mobility"
                          validate={[required]}
                          />{translate('extra-form.pmr')}
                      </label>
                  </div>
              </div>
              <div>
                <label>{translate('extra-form.gender')}</label>
                <div>
                    <label>
                        <Field
                        name="gender"
                        component="input"
                        type="radio"
                        value="male"
                        validate={[required]}
                        />{translate('extra-form.male')}
                    </label>
                    <label>
                        <Field
                        name="gender"
                        component="input"
                        type="radio"
                        value="female"
                        validate={[required]}
                        />{translate('extra-form.female')}
                    </label>
                </div>
            </div>
            </div>
            <div>
              <div>
                  <label>{translate('extra-form.type-of-workshop')}</label>
                  <div>
                      <label>
                          <Field
                          name="workshop-type"
                          component="input"
                          type="radio"
                          value="iPad"
                          validate={[required]}
                          />{translate('extra-form.ipad')}
                      </label>
                      <label>
                          <Field
                          name="workshop-type"
                          component="input"
                          type="radio"
                          value="collage"
                          validate={[required]}
                          />{translate('extra-form.collage')}
                      </label>
                  </div>
              </div>
              <div>
                <label>{translate('extra-form.localisation-of-the-workshop')}</label>
                <div>
                    <label>
                        <Field
                        name="workshop-localisation"
                        component="input"
                        type="radio"
                        value="interior"
                        validate={[required]}
                        />{translate('extra-form.interior')}
                    </label>
                    <label>
                        <Field
                        name="workshop-localisation"
                        component="input"
                        type="radio"
                        value="exterior"
                        validate={[required]}
                        />{translate('extra-form.exterior')}
                    </label>
                </div>
            </div>
            </div>
          </div>
          <NextButton disabled={invalid || submitting} onClick={this.onNextClick}/>
        </form>
      </Wrapper>
    )
  }
}


const validate = values => {
  const errors = {};

  if(isNaN(values.age)){
    errors.comment = 'Enter a correct age!';

  }

  if(!values.gender){
    errors.gender = 'Select a gender!';
  }

  if(!values.prm){
    errors.comment = 'Enter if prm or not!';
  }

  return errors;
}


const mapStateToProps = state => {
  return {
    initialValues:{},
    translate: getTranslate(state.locale),
    mix: state.mix,
    user: state.user
  }
}

const MixInformationsContainer  = connect(
    mapStateToProps,
    {saveMix, setMixUpload, setMixInformations}
)(MixInformations);

let InitializeFromStateForm = reduxForm({
  validate,
  form: 'MixInformationsForm',
})(MixInformationsContainer);

export default InitializeFromStateForm = connect(state => ({
}))(InitializeFromStateForm)


MixInformations.propTypes = {
  history: PropTypes.object
};
