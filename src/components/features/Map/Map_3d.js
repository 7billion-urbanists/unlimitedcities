/*
Path : /src/components/features/Map

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import randomPointsOnPolygon from 'random-points-on-polygon';
import { point } from '@turf/helpers';
import Featurecollection from 'turf-featurecollection';
import clustersDbScan from '@turf/clusters-dbscan';
import centroid from '@turf/centroid';
import { clusterEach, getCluster } from '@turf/clusters';
import turf_center from '@turf/center';
import circle from '@turf/circle';

// COMPONENTS
import ReactMapboxGl, { Layer, GeoJSONLayer } from "react-mapbox-gl";

import { MAPBOX_CONFIG, BOUNDING, LOCATION_DEFAULT } from '../../../config';
import styled from 'styled-components';


const Map = ReactMapboxGl(MAPBOX_CONFIG);


const Legend = styled.div`
    z-index: 9999;
    position: absolute;
    right: 10px;
    top: 69px;
    text-align: right;
    background: rgba(0,0,0,0.4);
    color: white;
    padding: 10px;
    border-radius: 2px;

    p{
        display: flex;
        align-items: center;
        justify-content: flex-end;
        padding-bottom: 2px;
        span{
            margin: 0 5px;
            width: 10px;
            height: 6px;
            display: inline-block;
            border-radius: 4px;
        }
    }
`;


class GalleryMap extends React.Component {

  constructor(props){
    super(props);
    this.geojson = {};
  }

  render(){

    const colors = [];
    colors[0] = "#00ff00";
    colors[1] = "#ff0000";
    colors[2] = "#0000ff";
    colors[3] = "#ffff00";

    const features = [];
    _.map((this.props.gallery.mixs), (value, index) => {
        const {lng, lat} = value.data.location;
        features.push(
            point([lng, lat], {...(value.data)})
        );
    });


    const statistics = {};

    this.props.library.data.map((category, index)=>{
        if(index >= this.props.library.data.length-1){
            return false;
        }
        let categoryName = category.id;
        statistics[categoryName] = 0;
        return false;
    });

    const featureCollection = Featurecollection(features);

    if(featureCollection.features.length > 0){
        var clustered = clustersDbScan(featureCollection, 0.1);
    
        let totalCluster = 0
        clusterEach(clustered, 'cluster', () => {
            totalCluster++;
        });

        this.geojson = {
            "type": "FeatureCollection",
            "features": [],
        }

        for(let i = 0; i < totalCluster; ++i){
            const cluster = getCluster(clustered, {cluster: i});
            const length = cluster.features.length;
            const center = turf_center(cluster);
            const radius = 0.001 * length;
            const options = {steps: 10, units: 'kilometers'};
            const area = circle(center, radius, options);
            const numberOfStatistics = _.size(statistics);
            const stems = randomPointsOnPolygon(length*numberOfStatistics, area);

            const stats = cluster.features.map((feature, index) => {
                const {serialized = null } = feature.properties;
                let mixStatistics = {...statistics};
                if(serialized){
                    const {objects} = serialized; 
                    if(objects){
                        _.map(objects, (obj)=>{
                            this.props.library.data.map((category)=>{
                                let categoryName = category.id;
                                category.collection.map((obj_c)=>{
                                    if(obj.filename===obj_c.filename){
                                        if(Number.isInteger(mixStatistics[categoryName]))
                                            mixStatistics[categoryName] += 1;
                                        return;
                                    }
                                    return;
                                })
                                return;
                            });
                        });
                    }
                }
                return mixStatistics;
            });

            stems.map((feature, index) => {
                const radius2 = 0.0005;
                const indexOfMixInCluster = Math.trunc(index/numberOfStatistics);
                const options = {
                    steps: 6,
                    units: 'kilometers',
                    properties: {
                        "height": _.valuesIn(stats[indexOfMixInCluster])[index%numberOfStatistics]*2,
                        "base_height": 0,
                        "color": _.toArray(colors)[index%numberOfStatistics],
                    }
                };

                this.geojson.features.push(circle([feature.geometry.coordinates[0], feature.geometry.coordinates[1]], radius2, options));
            });    
        }
        this.center = centroid(this.geojson);
    }else{
        return false;
    }




    return(
      <div>
        <Map
          center={this.center.geometry.coordinates}
          zoom={[16]}
          maxBounds={BOUNDING}
          pitch={[60]}
          style="mapbox://styles/mapbox/dark-v9"
          containerStyle={{
            height: "calc(100vh - 35px)",
            width: "100vw",
            direction: 'ltr',
          }}
        >
            {
                /*
                    <Layer
                        id="3d-buildings"
                        sourceId="composite"
                        sourceLayer={'building'}
                        type={'fill-extrusion'}
                        filter={['==', 'extrude', 'true']}
                        paint={{
                            'fill-extrusion-color': '#aaa',
                            'fill-extrusion-height': {
                                'type': 'identity',
                                'property': 'height'
                            },
                            'fill-extrusion-base': {
                                'type': 'identity',
                                'property': 'min_height'
                            },
                            'fill-extrusion-opacity': .6
                        }}
                    />
                */
            }
                
            <GeoJSONLayer
                id="test_src"
                data={this.geojson}
            />
            <Layer
                sourceId="test_src"
                type={'fill-extrusion'}
                paint={{
                    
                    // Get the fill-extrusion-color from the source 'color' property.
                    'fill-extrusion-color': ['get', 'color'],
                    
                    // Get fill-extrusion-height from the source 'height' property.
                    'fill-extrusion-height': ['get', 'height'],
                    
                    // Get fill-extrusion-base from the source 'base_height' property.
                    'fill-extrusion-base': ['get', 'base_height'],
                    
                    // Make extrusions slightly opaque for see through indoor walls.
                    'fill-extrusion-opacity': 1
                }}
            />
        </Map>
        <Legend>
            {
                _.map(_.keys(statistics), (value, key) => (
                    <p key={`legend-${key}`}>{value}: <span style={{background: colors[key],}}></span></p>
                ))
            }
        </Legend>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    gallery: state.gallery,
    library: state.library,
  }
}

export default connect(mapStateToProps)(GalleryMap);
