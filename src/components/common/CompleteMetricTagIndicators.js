/*
Path : /src/components/common

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MetricTags } from './Interface';
import { getTranslate } from 'react-localize-redux';
import _ from 'lodash';

const CompleteMetricTagIndicators = ({objects, fileNameToData, translate, className}) => {
  let metricTags = [];

  for (const obj of objects) {
    const objectMetricTags = fileNameToData[obj.filename].metricTags || [];
    for (const objectMetricTag of objectMetricTags) {
      metricTags.push(objectMetricTag)
    }
  }

  return (
    <div className={className}>
      <MetricTags metricTags={_.uniq(metricTags)} translate={translate}/>
    </div>
  )
}


CompleteMetricTagIndicators.propTypes = {
  objects: PropTypes.array.isRequired,
  fileNameToData: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
};


const MetricTagIndicatorsStyled = styled(CompleteMetricTagIndicators)`
  align-items: center;
`;

const mapStateToProps = (state, ownProps) => {
  return {
    fileNameToData: state.library.fileNameToData,
    translate: getTranslate(state.locale),
  }
}

export default connect(mapStateToProps)(MetricTagIndicatorsStyled);
