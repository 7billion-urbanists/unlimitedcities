/*
Path : /src/components/common

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import _ from 'lodash';
import { getActiveLanguage } from 'react-localize-redux';
import {LANGUAGES} from '../../config';


const Tag = styled.span`
  display: inline-block;
  background: #f1f1f1;
  color: #4c4c4c;
  font-size: 14px;
  margin-right: 5px;
  margin-bottom: 5px;
  border-radius: 4px;
  padding: 4px 6px;
`;

const Tags = ({objects, currentLanguage, fileNameToData, className}) => {
  const lang = LANGUAGES.find(e => (e.country === currentLanguage)).lang.toUpperCase();
  let currentTags = [];

  for (const obj of objects) {
    const tags = fileNameToData[obj.filename].tags;

    if(!tags){
      return;
    }

    const tagWithCurrentLanguage = tags[lang];
    if(tagWithCurrentLanguage[0]){
      tagWithCurrentLanguage.map(t => {
        currentTags.push(t);
      });   
    }
  }

  if(currentTags.length < 1){
    return false;
  }

  currentTags.sort((a, b) => {
    if(a < b) { return -1; }
    if(a > b) { return 1; }
    return 0;
  });

  return (
    <div className={className}>
      {
        _.uniq(currentTags).map(tag => (<Tag key={'tag-'+tag}>{tag}</Tag>))
      }
    </div>
  )
}

Tags.propTypes = {
  objects: PropTypes.array.isRequired,
  tags: PropTypes.object.isRequired,
};


const TagsStyled = styled(Tags)`
  padding-top:10px;
`;

const mapStateToProps = (state, ownProps) => {
  return {
    fileNameToData: state.library.fileNameToData,
    currentLanguage: getActiveLanguage(state.locale).code
  }
}

export default connect(mapStateToProps)(TagsStyled);
