/*
Path : /src/components/common

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { getTranslate } from 'react-localize-redux';
import { getActiveLanguage } from 'react-localize-redux';
import {APP_ID, LANGUAGES, METRIC_TAG_COLORS} from '../../config';

const categoryColors = [
  "#7EC72A",
  "#4A90E2",
  "#E69C2A",
  "#E62A2A"
];

const Bar = ({className, data, total, colors}) => {
  return(
    <div className={className}>
      {
        Object.entries(data).map(([_key, value], index) => {
          const width = (value/total) * 100;
          return (
            <div key={`bar-${index}`} style={{height:'100%', width: `${width}%`, backgroundColor: colors[index]}}></div>
          );
        })
      }
    </div>
  );
}

const BarStyled = styled(Bar)`
  margin: 0 auto;
  width: calc(100% - 40px);
  height: 4px;
  background-color: white;
  margin-top: 12px;
  border-radius: 2px;
  display: flex;
`;


const Legend = ({className, data, colors}) => {
  return(
    <div className={className}>
      {
         Object.entries(data).map(([key, _value], index) => (
            <div key={`bar-title-${index}`}><span style={{color: colors[index]}}>●</span> {key}</div>
          )
        )
      }
    </div>
  );
}

const LegendStyled = styled(Legend)`
  display: flex;
  margin: 0 auto;
  padding-top: 15px;
  justify-content: center;
  > div {
    margin: 0 8px;
  }
`;

const Statistics = ({mixs, fileNameToData, categories, translate, selectedMixs, currentLanguage, className}) => {
  const lang = LANGUAGES.find(e => (e.country === currentLanguage)).lang.toUpperCase();
  const statistics = {
    categories: Object.fromEntries(categories.map(category => [category[lang], 0])),
    metricTags: Object.fromEntries(Object.keys(METRIC_TAG_COLORS).map(metricTag => [metricTag, 0])),
    totalCategories: 0,
    totalMetricTags: 0,
  };

  const hasSelectedMixs = !!selectedMixs;
  selectedMixs = selectedMixs || Object.keys(mixs);

  for (const selectedMix of selectedMixs) {
    let mix = mixs[selectedMix.id || selectedMix];
    if(!mix.data.serialized || !mix.data.serialized.objects){
      continue;
    }
    for (const {filename} of mix.data.serialized.objects) {
      const data = fileNameToData[filename];
      const categoryName = data.category[lang];
      statistics.categories[categoryName] = statistics.categories[categoryName] + 1;
      statistics.totalCategories += 1;
      for (const metricTag of data.metricTags) {
        statistics.metricTags[metricTag] += 1;
        statistics.totalMetricTags += 1;
      };
    }
  }

  const hasStatisticCategories = !!Object.keys(statistics.categories).length;
  const hasStatisticMetricTags = !!Object.keys(statistics.metricTags).length;

  return(
    <div className={className}>
      <h2>
        {selectedMixs.length} {translate("mixs")}
      </h2>

      {!!hasStatisticCategories && (
        <div style={{marginTop: 20}}>
          {translate("object-division")}
          <BarStyled total={statistics.totalCategories} data={statistics.categories} colors={categoryColors}/>
          <LegendStyled data={statistics.categories} colors={categoryColors} />
        </div>
      )}
      
      {!!hasStatisticMetricTags && (
        <div style={{marginTop: 20}}>
          {translate("metric-tag-division")}
          <BarStyled
            total={statistics.totalMetricTags}
            data={statistics.metricTags}
            colors={Object.values(METRIC_TAG_COLORS)}
          />
          <LegendStyled data={Object.fromEntries(Object.entries(statistics.metricTags).map(([key, value]) => [translate(key), value]))} colors={Object.values(METRIC_TAG_COLORS)} />
        </div>
      )}
      <br />
      {!hasSelectedMixs && (<a
        style={{
          marginTop: 15,
          color: "#076171",
          fontStyle: "italic",
          fontSize: 14,
        }}
        href={`https://us-central1-unli-diy-1516007833309.cloudfunctions.net/getDataFromAppId?appId=${APP_ID}`}
        rel="noopener noreferrer"
        target='_blank'
      >
        {translate("download-csv-data")}
      </a>)}
    </div>
  );
};



Statistics.propTypes = {
  selectedMixs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      timestamp: PropTypes.number.isRequired
    })
  )
};


const StatisticsStyled = styled(Statistics)`
  font-size: 14px;
  color: #9B9B9B;
  text-align: center;
  h2{
    padding: 25px 0 5px;
    font-size: 28px;
    color: #9B9B9B;
  }
`;

const mapStateToProps = (state, ownProps) => {
  return {
    mixs: state.gallery.mixs,
    fileNameToData: state.library.fileNameToData,
    metricTags: state.library.metricTags,
    categories: state.library.categories,
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale).code
  }
}

export default connect(mapStateToProps)(StatisticsStyled);
