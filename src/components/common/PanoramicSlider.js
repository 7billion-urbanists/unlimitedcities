/*
Path : /src/components/common

Copyright (C) 2019 | Unlimited Cities® | Alain Renk | <alain.renk@7-bu.org>

Developer: Nicoals Ancel <email@adress>
Supported by: https://7billion-urbanists.org/ and https://freeit.world

This file is part of Unlimited Cities® software.

Unlimited Cities® is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Unlimited Cities® is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Unlimited Cities®. If not, see <http://www.gnu.org/licenses/>.
*/


import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/lab/Slider';

const styles = {
  root: {
    width: 800,
    position: 'absolute',
    transform: 'scaleX(-1)',
    left: 0, 
    top: -60,
  },
  slider: {
    padding: '22px 0px',
  },
  trackBefore: {
    opacity: 0.24,
  }
};

class SimpleSlider extends React.Component {

  state = {
    value: this.props.initPosX
  };
  
  componentDidMount(){
    this.setState({
      value: this.props.initPosX,
      mixId: this.props.mixId
    })
  }



  componentWillUpdate(nextProps){
    if(this.props.mix){
      if(this.props.mix.id !== nextProps.mix.id){
        this.setState({
          value: nextProps.initPosX
        });
      }
    }
  }

  handleChange = (e, value) => {
    this.props.onChangePositionX(-value+800);
    this.setState({ value: (-value+800) });
  };

  render() {
    const { classes, totalWidth } = this.props;
    const { value } = this.state;

    if(this.props.totalWidth<=800){
      return null;
    }
    return (
        <div className={classes.root}>
            <Slider
              classes={{ container: classes.slider, trackBefore: classes.trackBefore }}
              value={value}
              min={-(totalWidth/2)+800}
              max={totalWidth/2}
              aria-labelledby="label"
              onChange={this.handleChange}
            />
        </div>
    );
  }
}

SimpleSlider.propTypes = {
  classes: PropTypes.object.isRequired,
  onChangePositionX: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  return {
    totalWidth: state.mix.totalWidth,
    mix: state.gallery.selectedMix.mix,
  }
};

export default connect(mapStateToProps)(withStyles(styles)(SimpleSlider));